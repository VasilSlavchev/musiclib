 function validateForm() {
    var message="";
    var flag=true;
    var x = document.forms["myForm"]["semail"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        message="Not a valid e-mail address\n";
        flag=false;
    }
    var x = document.forms["myForm"]["name"].value;
    if (x == null || x == "") {
        message+="Name must be filled out\n";
        flag=false;
    }
   else if (/[^a-zA-Z0-9\-]/.test( x ))
{
    message+="Name can only contain alphanumeric characters and hypehns(-)\n";
    flag=false;
}
var x = document.forms["myForm"]["password"].value;
    if (!(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/.test( x ))) {
        message+="Password between 6 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter\n";
        flag=false;
    }
    if(flag==false)
    {
     alert(message);
     return false;
    }
}
function validateUpdateForm() {
    var message="";
    var flag=true;
    var x = document.forms["myUpdateForm"]["email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        message="Not a valid e-mail address\n";
        flag=false;
    }
    var x = document.forms["myUpdateForm"]["name"].value;
    if (x == null || x == "") {
        message+="Name must be filled out\n";
        flag=false;
    }
   else if (/[^a-zA-Z0-9\-]/.test( x ))
{
    message+="Name can only contain alphanumeric characters and hypehns(-)\n";
    flag=false;
}
    if(flag==false)
    {
     alert(message);
     return false;
    }
}
function validatePassword() {
var currentPassword,newPassword,confirmPassword,output = true;

currentPassword = document.frmChange.currentPassword;
newPassword = document.frmChange.newPassword;
confirmPassword = document.frmChange.confirmPassword;

if(!currentPassword.value) {
currentPassword.focus();
document.getElementById("currentPassword").innerHTML = "required";
output = false;
}

else if(!newPassword.value) {
newPassword.focus();
document.getElementById("newPassword").innerHTML = "required";
output = false;
}
else if(!confirmPassword.value) {
confirmPassword.focus();
document.getElementById("confirmPassword").innerHTML = "required";
output = false;
}
if(newPassword.value != confirmPassword.value) {
newPassword.value="";
confirmPassword.value="";
newPassword.focus();
document.getElementById("confirmPassword").innerHTML = "not same";
output = false;
} 	
  if(!(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/.test(newPassword.value))) {
                                                   document.getElementById("newPassword").innerHTML ="Password between 6 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter";
                                                   output = false;

                                           }
return output;
}

function validateemail() {
var emailid,atpos,dotpos,output = true;

emailid = document.frmEmail.email;
atpos = emailid.value.indexOf("@");
 dotpos = emailid.value.lastIndexOf(".");

if(!emailid.value) {
emailid.focus();
document.getElementById("sendEmail").innerHTML = "required";
output = false;
}
else if(atpos< 1 || dotpos<atpos+2 || dotpos+2>=emailid.length) {
  document.getElementById("sendEmail").innerHTML = "Enter Valid Email Id";
        output = false;
    }
return output;
}
