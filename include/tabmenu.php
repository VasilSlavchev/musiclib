<?php
require 'db_connection.php';
class tabmenu{
     private $email;
    private $name;
    private $password;
    private $result;
    private $numResults;
    private $message;
    private $user_id;
    private $user_name;
    private $path;
    private $msgflag;
    private $passwordflag;
    private $Updatedmessage;
    private $Updatedmessageflag;
    private $newPassword;
    private $subject;
 
            function _construct(){
         $this->path="/musiclib/uploads/images/";
    }
            function createtabmenu(){
        echo '<div class="content">
                 <ul class="nav nav-tabs" role="tablist">
                     <li id="hometab" class="active"><a href="#home" role="tab" data-toggle="tab">Home</a></li>
                     <li id="logintab" class="dropdown"><a href="#login" role="tab" data-toggle="tab">';
                     if(isset($_SESSION["user_name"])){
                         echo 'Profile';
 } 
                     else { echo 'Login';} 
                     echo '</a></li>';
                     if(!isset($_SESSION["user_name"])) { echo '<li><a href="#signup" role="tab" data-toggle="tab">Sign Up</a></li>';
                        }
                     if(isset($_SESSION["user_name"])) { echo' <li><a href="#upload_music" role="tab" data-toggle="tab">Upload Music</a></li>';
                      } 
                     echo '<li class=""><a href="#contact" role="tab" data-toggle="tab">Contact</a></li></ul>';
    }
    function tabcontent(){
        $this->homepage();
        $this->signin();
        $this->signup();
        $this->contact();
    }
    function signin(){
        /* Start update profile function */
        $this->Updatedmessageflag="false";
          if(isset($_POST['action']) && $_POST['action']=="update"){

                        $db = new db_connection;
                        $db->connect();
                  
                        $this->name       = $db->escapeString($_POST['name']);
                        $this->email      = $db->escapeString($_POST['email']);
                          
        
                        $db->select('users','email','email="'.$this->email.'" and id<>'.$_SESSION["user_id"].''); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
                        $this->result = $db->getResult();
                        $this->numResults = $db->numRows();
  
                     if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) // Validate email address
                       {
                         $this->Updatedmessage =  "Invalid email address please type a valid email!!";
                         $this->Updatedmessageflag="true";
                        }
                       elseif($this->numResults>=1)
                      {
                       $this->Updatedmessage = $this->email." Email already exist!!";
                       $this->Updatedmessageflag="true";
                     }
                  else if(isset($_FILES['file']['name']) && empty($_FILES['file']['name'])){
                $db->update('users',array('name'=>$this->name,'email'=>$this->email),'id='.$_SESSION["user_id"].''); 
                                              $this->Updatedmessage = "Updated Sucessfully!!";
                                              $this->Updatedmessageflag="true";

       }                
             else
                  {
         
                        $allowExtention=array('jpg','png','jpeg');
                        $fileExtention=explode('.',$_FILES["file"]["name"]);
                        $extention=end($fileExtention);

                        if(($_FILES['file']['type']=='image/jpg' 
                           ||$_FILES['file']['type']=='image/jpeg'     
                           ||$_FILES['file']['type']=='image/pjpeg'     
                           ||$_FILES['file']['type']=='image/pjpg'     
                           ||$_FILES['file']['type']=='image/png'     
                           ||$_FILES['file']['type']=='image/x-png')
                           && in_array($extention,$allowExtention)        )
                           {
                            if(!$_FILES['file']['error']>0)
                            {
                                move_uploaded_file($_FILES['file']['tmp_name'],"uploads/images/".$_FILES["file"]["name"]);
                               // $image=$_FILES["file"]["name"];
                                $this->image=$_FILES["file"]["name"];
                                $db->update('users',array('name'=>$this->name,'email'=>$this->email,'image'=>$this->image),'id='.$_SESSION["user_id"].''); // Table name, column names and values, WHERE

                                $this->Updatedmessage = "Update data Sucessfully!!";
                                $this->Updatedmessageflag="true";

                            }
                            else{
                                   $this->Updatedmessage = "Error".$_FILES['file']['error'];
                                   $this->Updatedmessageflag="true";
                                 }
                              }

                         else{
                                            
                              $this->Updatedmessage = "Invalid File";
                                              $this->Updatedmessageflag="true";
        }
           
              
        }
         if($this->Updatedmessageflag=="true"){
                               echo '<script> 
                                 $(document).ready(function(){
                                 $("#home").removeClass().addClass("tab-pane");
                                 $("#login").removeClass().addClass("tab-pane active");
                                 $("#general").removeClass().addClass("tab-pane");
                                 $("#Edit").removeClass().addClass("tab-pane active");
                                 $("#generalingo").removeClass();
                                 $("#Edittab").removeClass().addClass("active");
                             
                         });    
                                 </script>';
                     }
                                     
                 
  //echo  $this->Updatedmessage;
  }
        /* End update profile function */
        
        /* Start password reset function */
$message="";
$this->passwordflag=false;
        if(isset($_POST['action']) && $_POST['action']=="changepassword"){

                        $db = new db_connection;
                        $db->connect();
                  
                        $this->password= $db->escapeString($_POST["currentPassword"]);
                        $this->newPassword= $db->escapeString($_POST["newPassword"]);
                        $this->password=md5($this->password);
                        $this->newPassword=md5($this->newPassword);
                        $db->select('users','name,id,password',' id = "'.$_SESSION["user_id"].'"  '); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
                        $this->result = $db->getResult();
                        $this->numResults = $db->numRows();
       
                 
                        if($this->password == $this->result["password"]) {
                               // mysql_query("UPDATE users set password='" . md5($_POST["newPassword"]) . "' WHERE id='" . $_SESSION["user_id"] . "'");
                                $db->update('users',array('password'=>$this->newPassword),'id='.$_SESSION["user_id"].''); // Table name, column names and values, WHERE
                               $message = "Password Changed";
                               $this->passwordflag="true";
                        } 
                        else  {
                            $message = "Current Password is not correct";
                            $this->passwordflag="true";
                        }
                     }
                     if($this->passwordflag=="true"){
                                 echo '<script> 
                                 $(document).ready(function(){
                                 $("#home").removeClass().addClass("tab-pane");
                                 $("#login").removeClass().addClass("tab-pane active");
                                 $("#general").removeClass().addClass("tab-pane");
                                 $("#Password").removeClass().addClass("tab-pane active");
                                 $("#generalingo").removeClass();
                                 $("#changepass").removeClass().addClass("active");
                                 

                               
                                 
                         });    
                                 </script>';
                     }
                                
                       
/* End password reset sunction */
 /* Start Login function */
         if(isset($_POST['action']))
          {          
   
                if($_POST['action']=="login")
                {
                        $db = new db_connection;
                        $db->connect();
                        $this->email = $db->escapeString($_POST["email"]);
                        $this->password= $db->escapeString($_POST["password"]);

                        $this->password=md5($this->password);
                        $db->select('users','name,id',' email = "'.$this->email.'" AND PASSWORD = "'.$this->password.'"'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
                        $this->result = $db->getResult();
                        $this->numResults = $db->numRows();

                         if(($this->numResults)>=1)
                            {
                              $_SESSION["user_id"] = $this->result[id];
                              $_SESSION["user_name"] = $this->result[name];
                             }
                        else
                            {
                                 echo '<script> alert("Invalid email or password!!");
                                 $(document).ready(function(){
                                 $("#home").removeClass().addClass("tab-pane");
                                 $("#login").removeClass().addClass("tab-pane active");
                                 $("#hometab").removeClass();
                                 $("#logintab").removeClass().addClass("active");
                                 
                         });    
                                 </script>';
                                 
                            }     
                        if(isset($_SESSION["user_id"])) {
                               header("Location:musiclib.php");
                            }
                    }
          }
                echo '<div class="tab-pane" id="login"> ';
          if(isset($_SESSION["user_name"])){
               
          $db = new db_connection;
          $db->connect();
          $this->user_name       = $db->escapeString($_SESSION["user_name"]);
          $this->user_id      = $db->escapeString($_SESSION["user_id"]);
        
     
        
        $db->select('users','name,id,image,email','id='.$this->user_id.''); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
        $this->result = $db->getResult();
        $this->numResults = $db->numRows();

        if(($this->numResults)>=1)
        {
             $this->path="/musiclib/uploads/images/";
             echo '<nav class="navbar navbar-inverse" role="navigation"><ul class="nav nav-pills">
  <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li id="generalingo" class="active"><a href="#general" role="tab" data-toggle="tab">General Information</a></li>
  <li id="Edittab" class=""><a href="#Edit" role="tab" data-toggle="tab">Edit</a></li>
  <li id="changepass" class=""><a href="#Password" role="tab" data-toggle="tab">Change Password</a></li>
  <li id="subscription" class=""><a href="#paypal" role="tab" data-toggle="tab">Donation</a></li>
  <li style="float: right; display: inline-flex;margin-top:5px"><img src="'.$this->path.$this->result['image'].'" alt="profile" class="img-circle" height="30" width="30">
      <div class="dropdown" style="margin-right: 5px">
  <a data-toggle="dropdown" href="#">'.$_SESSION["user_name"].'</a>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
    <li><a href="include/logout.php" tite="Logout"><span class="glyphicon glyphicon-off"> Logout</span></a></li>
  </ul>
</div></li>
</ul></nav>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="general">';
    $this->path="/musiclib/uploads/images/";
            echo '<div class="divtabel" style="width:100%">';
            echo '<div style="width:25%"></br><img src="'.$this->path.$this->result['image'].'" alt="profile" class="img-circle" height="200" width="200"></div>';
             echo '<div style="width:75%">';
            
            echo '</br>User Id :'.$this->result['id'];
            echo '</br><span class="glyphicon glyphicon-user"></span> Name :'.$this->result['name'];
            echo '</br><span class="glyphicon glyphicon-envelope"></span> Email :'.$this->result['email'];
            echo '</div></div>';
echo '</div>
  <div class="tab-pane" id="Edit">';
           
              echo '<div class="message">'.$this->Updatedmessage.'</div>';
              echo '<form name="myUpdateForm" action="" method="post" enctype="multipart/form-data" onsubmit="return validateUpdateForm()">';
            echo '</br>User Id :'.$this->result['id'];
             echo '</br></br>Name :<input name="name" type="text" value="'.$this->result['name'].'">';
            echo '</br></br>Email :<input name="email" type="text" value="'.$this->result['email'].'">';
             echo '</br><img src="'.$this->path.$this->result['image'].'" alt="profile" class="img-circle" height="200" width="200">';
            echo '</br></br><label for="file">Change Your Image</label>
                    <input type="file" name="file" id="file"><br>
                    <input name="action" type="hidden" value="update" /></p></br>
                    <p><input  class="btn btn-success" type="submit" value="Update" /></p>
                    </form>';
echo '</div>
      <div class="tab-pane" id="paypal">';
           
          
$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id='payment@musiclibrary.com'; // Business email ID

echo '<h4>Welcome,'.$_SESSION["user_name"].'</h4>
 <p>We are free music event organiser on most of cities for supporting to social activity</br>
 You can also become a part of our social servises</br>
 Donate and be part of social activity</p>
<div class="product">            

    <div class="btn">
    <form action="'.$paypal_url.'" method="post" name="frmPayPal1">
    <input type="hidden" name="business" value="'.$paypal_id.'">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="item_name" value="Online Music Library Club">
    <input type="hidden" name="item_number" value="1">
    <input type="hidden" name="credits" value="510">
    <input type="hidden" name="userid" value="1">
    <lable>Enter Donation Amount</lable></br>
    <input type="text" name="amount" value="10"><span class="glyphicon glyphicon-usd"></span></br>';
    echo "<input type='hidden' name='cpp_header_image' value='http://$_SERVER[HTTP_HOST]/musiclib/assets/images/musiclib_logo.jpg'>";
    echo '<input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="handling" value="0">
    <input type="hidden" name="cancel_return" value="http://localhost/musiclib/payment_gatway/cancel.php">
    <input type="hidden" name="return" value="http://localhost/musiclib/payment_gatway/success.php">';
    echo "<input type='image' src='http://$_SERVER[HTTP_HOST]/musiclib/assets/images/paypaldonation.png' border='0' name='submit' alt='PayPal - The safer, easier way to pay online!'>";
    echo "<img alt='' border='0' src='http://$_SERVER[HTTP_HOST]/musiclib/assets/images/paypaldonation.png' width='1' height='1'>";
    echo '</form>
    </div>
</div>';
echo '</div>
  <div class="tab-pane" id="Password">
  <form name="frmChange" method="post" action="" onSubmit="return validatePassword()">
                    <div style="width:500px;">
                    <div class="message">';
                    if(($this->passwordflag =="true"))
                        { echo $message;
                     } 
                    echo '</div>
                    <table border="0" cellpadding="10" cellspacing="0" width="500" align="center" class="tblSaveForm">
                    <tr class="tableheader">
                    <td colspan="2">Change Password</td>
                    </tr>
                    <tr>
                    <td width="40%"><label>Current Password</label></td>
                    <td width="60%"><input type="password" name="currentPassword" class="txtField"/><span id="currentPassword"  class="required"></span></td>
                    </tr>
                    <tr>
                    <td><label>New Password</label></td>
                    <td><input type="password" name="newPassword" class="txtField"/><span id="newPassword" class="required"></span></td>
                    </tr>
                    <td><label>Confirm Password</label></td>
                    <td><input type="password" name="confirmPassword" class="txtField"/><span id="confirmPassword" class="required"></span></td>
                    </tr>
                    <tr>
                    <td colspan="2">
                    <input name="action" type="hidden" value="changepassword" />
                    <input type="submit" name="submit" value="Submit" class="btnSubmit"></td>
                    </tr>
                    </table>
                    </div>
                    </form>
  
</div>

</div>';
            
          
           
            
           
        }
      }
      else {
      echo '<form action="" method="post">
          </br><label for="email">Username</label></br>
         <p><input id="email" name="email" type="text" placeholder="Email"></p>
         </br><label for="password">Password</label></br>
        <p><input id="password" name="password" type="password" placeholder="Password">
        <input name="action" type="hidden" value="login" /></p></br>
        <p><input  class="btn btn-primary" type="submit" value="login" /></p>
      </form>
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
         Forgot password
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">
      <form action="../musiclib/forgot.php" method="post">
        Enter you email ID: <input type="text" name="email">
        <input type="submit" name="submit" value="Send">
       </form>
      </div>
    </div>
  </div>      
 </div>
';
       }
  echo '</div>';
    }
    function signup(){
        $message="";
        $this->message="";
        $this->flag="false";
        if(isset($_POST['action']))
        if($_POST['action']=="signup")
       {
        $db = new db_connection;
        $db->connect();
        $this->name       = $db->escapeString($_POST['name']);
        $this->email      = $db->escapeString($_POST['email']);
        $this->password   = $db->escapeString($_POST['password']);
     
        
        $db->select('users','email','email="'.$this->email.'"'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
        $this->result = $db->getResult();
        $this->numResults = $db->numRows();
  
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) // Validate email address
           {
            $this->message =  "Invalid email address please type a valid email!!";
            $this->flag="true";
            }
           elseif($this->numResults>=1)
           {
            $this->message = $this->email." Email already exist!!";
            $this->flag="true";
           }
        
        else
        {
         
            $allowExtention=array('jpg','png','jpeg');
            $fileExtention=explode('.',$_FILES["file"]["name"]);
             $extention=end($fileExtention);

            if(($_FILES['file']['type']=='image/jpg' 
               ||$_FILES['file']['type']=='image/jpeg'     
               ||$_FILES['file']['type']=='image/pjpeg'     
               ||$_FILES['file']['type']=='image/pjpg'     
               ||$_FILES['file']['type']=='image/png'     
               ||$_FILES['file']['type']=='image/x-png')
               && in_array($extention,$allowExtention)        )
               {
                if(!$_FILES['file']['error']>0)
                {
                    move_uploaded_file($_FILES['file']['tmp_name'],"uploads/images/".$_FILES["file"]["name"]);
                   // $image=$_FILES["file"]["name"];
                    $this->image=$_FILES["file"]["name"];
                    
                    $db->insert('users',array('name'=>$this->name,'email'=>$this->email,'password'=>md5($this->password),'date'=>"2014-07-10 17:52:11",'image'=>$this->image));  // Table name, column names and respective values
                    $this->message = "Signup Sucessfully!!";
                    $this->flag="sucessfull";

                 }
             else{
                    $this->message = "Error".$_FILES['file']['error'];
                    $this->flag="true";
                  }
               }

          else{
                   $this->image="defualt.jpg";
                    
                    $db->insert('users',array('name'=>$this->name,'email'=>$this->email,'password'=>md5($this->password),'date'=>"2014-07-10 17:52:11",'image'=>$this->image));  // Table name, column names and respective values
                    $this->message = "Signup Sucessfully!!";
                    $this->flag="sucessfull";
         }
           
        }
       }
     $message=$this->message;
   
          if(!isset($_SESSION["user_name"])) {
              $this->name       ="";
              $this->email      ="";
              $this->password   ="";
              if(isset($_POST['name']))
              $this->name       = ($_POST['name']);
              if(isset($_POST['email']))
              $this->email      = ($_POST['email']);
              if(isset($_POST['password']))
              $this->password   = ($_POST['password']);
              if($this->flag=="sucessfull"){
                  $this->name       ="";
                  $this->email      ="";
                  $this->password   ="";
              }
              
         echo '<div class="tab-pane" id="signup">
         <div style="max-width:980px;width:100%;display: inline-flex;">                 
          <div style="width:50%;border:1px solid #5BB75B;padding-left:10px;background:#B3D4FC">
                 
                 <div style="max-width:478px;background:#5BB75B;height:50px;text-align:center;vertical-baseline:center"><h2>Register</h2></div> 
                <span style="color:red">'.$message.'</span>
               <form name="myForm" action="" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                  </br><label for="name">*Name</label></br>
                    <p><input id="name" name="name" type="text" value="'.$this->name.'" placeholder="Name"></p>
                    </br><label for="semail">*Email</label></br>
                    <p><input id="semail" name="email" type="text" value="'.$this->email.'" placeholder="Email"></p>
                    </br><label for="spassword">*Password</label></br>
                    <p><input id="spassword" name="password" type="password" value="'.$this->password.'" placeholder="Password"></br>
                    <label for="file">Upload Your Image</label>
                    <input type="file" name="file" id="file"><br>
                    <input name="action" type="hidden" value="signup" /></p></br>
                    <p><input  class="btn btn-primary" type="submit" value="Signup" /></p>
                    </form>
                    </div>
                   <div style="width: 50%; padding: 25px;">Sign up here free!!!</br> Here you can create your account. For upload mp3 music file you need to login in to your account. If you are not member of this site then create membership here</br>If you have already register then click on login tab</div><p><ul>Before sign up remenmer following things

<li>Name must be filled out</li>
<li>Name can only contain alphanumeric characters and hypehns(-)</li>
<li>Password between 6 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter</li>
<li>Upload Image. image file should be only in png,jpg,jpeg format</li>
<li>* Indicate Compalsary field.</li>
</ul></p>
                    </div></div>';
     if($this->flag=="true" ||$this->flag=="sucessfull"){
        echo '<script>$(document).ready(function(){
                                 $("#home").removeClass().addClass("tab-pane");
                                 $("#signup").removeClass().addClass("tab-pane active");
                                 $("#hometab").removeClass();
                                 
                                 
                         });    
                                 </script>';
    }
         } 
    }
     function contact(){
                 $message="";  
         if(isset($_POST['action']) && $_POST['action']=="uploadmusic")
             {
              
                $allowExtention=array('mp3', 'mp4', 'wav');
                $fileExtention=explode('.',$_FILES["musicfile"]["name"]);
                $extention=end($fileExtention);

                if(in_array($extention,$allowExtention))
                 {    
                  if(!$_FILES['musicfile']['error']>0)
                   {   
                      move_uploaded_file($_FILES['musicfile']['tmp_name'],"uploads/music/".$_FILES["musicfile"]["name"]);
                      $image=$_FILES["musicfile"]["name"];
                      $message = "Uploaded Sucessfully!!";
  
                    }
                 else{    
                $message = "Error".$_FILES['musicfile']['error'];
               }
             }
              else{    
             $message = "Invalid File Type(only upload mp3,mp4 and wav file)";
           }
             }
             if($message!=""){

             echo '<script>$(document).ready(function(){
                                 $("#home").removeClass().addClass("tab-pane");
                                 $("#upload_music").removeClass().addClass("tab-pane active");
                                 $("#hometab").removeClass();
                                 
                                 
                         });    
                                 </script>';
                            
             }
                 echo '<div class="tab-pane" id="upload_music">
                 <p>You can Upload music file here</p>
                    <p class="text-success">It\'s Free</p>

                    <form name="myFormupload" action="" method="post" enctype="multipart/form-data">

                        <input type="file" name="musicfile" id="musicfile"><br>
                        <input name="action" type="hidden" value="uploadmusic" /></p></br>
                    
                        <p><input  class="btn btn-primary" type="submit" value="Upload" /></p>
                     </form>
                     <p class="text-success">'.$message.'</p>
                      </div>
                      <div class="tab-pane" id="contact">
                      <div style="width:100%;display: inline-flex;">
                      <div style="width:50%">
                      <h2>Contact Us</h2>
                      <!-- Button trigger modal -->
                      
                      
                    <p>Office #1, Mahavir Park Complex,</br>
                    Pune Satara Road, Pune - 37, MH, India</p></br>
                  

     <div id="map_canvas"></div>

 </div>
<div style="width:50%">
                      </br><h2>Mail Us</h2>
                    
        <form name="frmEmail" action="" method="post" onsubmit="return validateemail()">
        </br><lable>Name</lable>        
        </br><span class="glyphicon glyphicon-user"></span>
        <input type="text" name="name" value="" placeholder="Your Name">
        </br><lable>Email Id</lable>
        </br><span class="glyphicon glyphicon-envelope"></span>
        <input type="text" name="email" value="" placeholder="Your Email Id"><span id="sendEmail" class="required"></span>
        </br><lable>Subject</lable>
        </br><span class="glyphicon glyphicon-pencil"></span><input type="text" name="subject" value="" placeholder="Subject">
        </br><lable>Message</lable>
        </br><textarea rows="5" cols="50" name="message" value="" placeholder="Enter Yor Message Here"></textarea>
       
        </br><input type="submit" class="btn btn-success" name="sendemail" value="Send">
        </form>
        </div>
        </div>
                        </div>

              </div>

        </div>';
                 /* Start Send Email Function*/
                 if(isset($_POST['sendemail'])) {
                     $this->name="";
                              $this->subject="Contact";
                              $this->message="Hi";
                                if(isset($_POST['name']))
                                {
                                 $this->name=$_POST['name'];                               
                                } 
                                
                                if(isset($_POST['subject']))
                                {
                                    $this->subject=$_POST['subject'];                               
                                } 
                                if(isset($_POST['message']))
                                {
                                    $this->message=$_POST['message'];                               
                                } 
                                if(isset($_POST['email']))
                                {
                                    $headers =  "From: ".$_POST['email']."\r\n" . 
                                 mail("prashant.walke@perennialsys.com",$this->subject, $this->message,$headers);
                                }
                               
                 }
                         
                 /*End send mail function */
                                  
     }
    
    function homepage(){
        echo '<div class="tab-content">
                 <div class="tab-pane active" id="home">
                      <p></br>Music Lib is Online Mp3 songs gallary. Whewre you can find mp3 song.</p>
                      <p>You can listen online song without any cost. It is free of cost</p>
                      <p> If you want dounload any music file then you need to login in to your account and click on dounload link.</br> click on login tab to free download</br>
                      If you do not have account then dont mind. you can register on Music Library site without any subscription. It all Free!</br> 
                      Click on Sign Up tab for resister your account</p>
                      <p>We have organise many saong event that\'s why we are maintain member account for sending mail to our member with event details.</br> 
                      so you need to only your name and mail id for register your membership on our site</br>
                      Just Click in sign up->register and enjoy!!!!</p>
                        
                      <p class="text-success">Just Click on!!!! And Enjoy!!!!!</p>';
                      $dir    = dirname(__FILE__)."\..\uploads\music";
                      $files = scandir($dir);
                       
                       $i=1;
                       foreach($files as $file)
                      {
                        if(($file != '.' && $file != '..' )&&($file!='index.php')&&($file!='.htaccess')){
                       echo '<table class="table table-striped">';
                       echo '<tr';
                       if($i%2==0) echo 'class="success"';
                       echo'>
                            <td style="width:10%">'.$i.'</td>
                            <td style="width:30%">'.$file.'</td>
                            <td style="width:35%"><audio controls> <source src="uploads/music/'.$file.'" type="audio/ogg">
                                Your browser does not support the audio element.
                             </audio></td>';
                            if(isset($_SESSION["user_id"])) {                       
                             echo' <td><a href="http://localhost/musiclib/include/download.php?id='.$file.'"><span class="glyphicon glyphicon-download-alt"> Download</span></a></td>';
                             }    
                             echo '</tr>
                           </table>';
                       $i++;
                      }
                   }
                   echo ' </div>';
    }
}
    
$tm= new tabmenu;
$tm->createtabmenu();
$tm->tabcontent();
?>



    
   